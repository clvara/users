import Personne from './Components/Personne/Personne';
import Header from './Components/Header/Header';
import Table from './Components/Table/Table';

import { useEffect, useState } from 'react';
import uuid from "react-uuid";

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';


function App() {

  // =================STATES=================== //
  const [users, setUsers] = useState([]);
  const [isMale, setIsMale] = useState('male');
  // ========================================== //

  const onCheckbox = (event) => {
    setIsMale(event.target.value);
  }

  // =================FETCH=================== //
  useEffect(() => {
    const args = new URLSearchParams();

    args.set('results', 10);
    args.set('gender', isMale);

    (async function fetchData() {
      await fetch(`https://randomuser.me/api/?${args}`, { mode: 'cors' })
      .then(res => (res.ok ? res.json() : Promise.reject(res)))
      .then(res => {

        //Si le JSON est au bon format
        if (res.results !== undefined) {
          res = res.results;

          //On map nos données
          setUsers(res.flatMap(u => {
            return { ...u, id: uuid() }
          }).map(u =>
            <Personne
              key={u.id}
              img={u.picture.thumbnail}
              nom={`${u.name.title} ${u.name.first} ${u.name.last}`}
              adresse={`${u.location.city} - ${u.location.state}`}
              code={u.nat}
              pays={u.location.country}
              age={u.dob.age}
              email={u.email}
              telephone={u.phone}
            />
          ));
        } else {
          console.error("Erreur lors du fetch de L'API");
        }
      });
    })();

  }, [isMale]);

  // ========================================== //

  return (
    <section>
      <Header />

      {/* Conteneur principal */}
      <main role="main" className="container main-app">
        <div className="jumbotron">
          <h1>Users</h1>

          <form className="mb-3">

            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="check_male" value="male" id="radioMale" onChange={onCheckbox} defaultChecked />
              <label className="form-check-label" htmlFor="radioMale">
                Male
                </label>
            </div>
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="check_male" value="female" id="radioFemale" onChange={onCheckbox} />
              <label className="form-check-label" htmlFor="radioFemale">
                Female
              </label>
            </div>

          </form>

          <Table className="lead" data={users} />
        </div>
      </main>

    </section>
  );
}

export default App;
