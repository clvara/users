import 'bootstrap/dist/css/bootstrap.min.css';
import './Header.css'

import { useEffect, useState } from 'react';

function Header({ onResearch }) {

    // =================STATES=================== //
    const [searchText, setSearchText] = useState("");
    const [data, setData] = useState([]);
    // ========================================== //

    const onSearch = (text) => {
        if (text !== searchText) {
            setSearchText(text);
        }
    }

    const onClick = (event) => {
        event.preventDefault();

        if (searchText !== "") {
            //onResearch(searchText);
        }
    }

    // =================FETCH=================== //
    useEffect(() => {

        (async function fetchData() {
            await fetch(`https://restcountries.eu/rest/v2/name/${searchText}`)
            .then(res => (res.ok ? res.json() : Promise.reject(res)))
            .then(res => {
                setData(res.map(country =>
                    <option key={country.alpha2Code} data-id={country.alpha2Code} value={country.name} />
                ));
            });
        })();

    }, [searchText]);
    // ========================================== //

    return (
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <span className="navbar-brand">Users</span>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarCollapse">
                <form className="form-check form-check-inline">
                    <input type="text"
                        list="data"
                        className="form-control mr-sm-2 searchbar"
                        placeholder="Rechercher"
                        id="searchbar"
                        onChange={event => onSearch(event.target.value,)}
                    />

                    <button type="button"
                        className="btn btn-outline-success my-2 my-sm-0"
                        onClick={onClick}
                    >Rechercher</button>

                    <datalist id="data">
                        {data}
                    </datalist>
                </form>
            </div>
        </nav>
    );
}

export default Header;