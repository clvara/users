import './Personne.css'

function Personne({img, nom, adresse, code, pays, age, email, telephone}) {
    
    return (
        <tr>
            <th scope="row">
                <img src={img} className="personne" alt="thumbnail"/>
            </th>
            <td>{nom}</td>
            <td>{adresse}</td>
            <td>{pays} <img src={`https://www.countryflags.io/${code}/flat/32.png`} className="flag" alt="flag"/></td>
            <td>{age}</td>
            <td>{email}</td>
            <td>{telephone}</td>
        </tr>
    );
}

export default Personne;