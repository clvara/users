import 'bootstrap/dist/css/bootstrap.min.css';
import './Table.css'

function Table({data}) {

    return (
        <table className="table table-striped">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">City - State</th>
                    <th scope="col">Country</th>
                    <th scope="col">Age</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                </tr>
            </thead>

            <tbody>
                {data}
            </tbody>

        </table>
    );
}

export default Table;